import {
    Pawn
} from "./Pawn"
import {
    Player
} from "./Player"
const row10 = document.querySelectorAll('#row10 > td');
const row10Arr = Array.prototype.slice.call(row10);

const row9 = document.querySelectorAll('#row9 > td');
const row9Arr = Array.prototype.slice.call(row9);

const row8 = document.querySelectorAll('#row8 > td');
const row8Arr = Array.prototype.slice.call(row8);

const row7 = document.querySelectorAll('#row7 > td');
const row7Arr = Array.prototype.slice.call(row7);

const row6 = document.querySelectorAll('#row6 > td');
const row6Arr = Array.prototype.slice.call(row6);

const row5 = document.querySelectorAll('#row5 > td');
const row5Arr = Array.prototype.slice.call(row5);

const row4 = document.querySelectorAll('#row4 > td');
const row4Arr = Array.prototype.slice.call(row4);

const row3 = document.querySelectorAll('#row3 > td');
const row3Arr = Array.prototype.slice.call(row3);

const row2 = document.querySelectorAll('#row2 > td');
const row2Arr = Array.prototype.slice.call(row2);

const row1 = document.querySelectorAll('#row1 > td');
const row1Arr = Array.prototype.slice.call(row1);

const chessBoard = [row10Arr, row9Arr, row8Arr, row7Arr, row6Arr, row5Arr, row4Arr, row3Arr, row2Arr, row1Arr];
const everyCells = document.querySelectorAll('td');

let blackPlayer = new Player(chessBoard, 'black');
let whitePlayer = new Player(chessBoard, 'white');
let players = [blackPlayer, whitePlayer];
players[0].createPawn(0, 1);
players[0].createPawn(0, 3);
players[0].createPawn(0, 5);
players[0].createPawn(0, 7);
players[0].createPawn(0, 9);


players[1].createPawn(6, 6);

let index = 0;

function play() {
    for (let pawn of players[index].pawns) {
        pawn.htmlElement.addEventListener('click', () => {
            pawn.firstClick();
            for (let i = 0; i < everyCells.length; i++) {
                everyCells[i].addEventListener('click', (event) => {
                    if (event.target.classList.contains('possibleMove')) {
                        if (event.target === pawn.possibleMove1) {
                            pawn.move1();
                            switchPlayer();
                        } else if (event.target === pawn.possibleMove2) {
                            pawn.move2();
                            switchPlayer();
                        } else if (event.target === pawn.possibleTake1) {
                            pawn.take1();
                            switchPlayer();
                        } else if (event.target === pawn.possibleTake2) {
                            pawn.take2();
                            switchPlayer();
                        }
                    }
                })
            }
        })
    }
}


function switchPlayer() {
    if (index === 0) {
        index++;
    } else {
        index = 0;
    }
    play()
}

play()

// function play () {
//     players[index].pawn.htmlElement.addEventListener('click', () => {
//         players[index].pawn.firstClick();
//         if (players[index].pawn.possibleMove1.hasChildNodes()){
//             players[index].pawn.possibleTake1.addEventListener('click', ()=>{
//                 players[index].pawn.take1();
//                 if (index===0) {
//                     index++;
//                 }else {
//                     index = 0;
//                 }
//                 play()
//             }, {once:true});
//         } else {
//             players[index].pawn.possibleMove1.addEventListener('click', () => {
//                 players[index].pawn.move1();
//                 if (index===0) {
//                     index++;
//                 }else {
//                     index = 0;
//                 }
//                 play()
//             }, {once : true});
//         }

//         if (players[index].pawn.possibleMove2.hasChildNodes()){
//             players[index].pawn.possibleTake2.addEventListener('click', ()=>{
//                 players[index].pawn.take2();
//                 if (index===0) {
//                     index++;
//                 }else {
//                     index = 0;
//                 }
//                 play()
//             }, {once:true});
//         } else {
//             players[index].pawn.possibleMove2.addEventListener('click', () => {
//                 players[index].pawn.move2();
//                 if (index===0) {
//                     index++;
//                 }else {
//                     index = 0;
//                 }
//                 play()
//             }, {once : true});
//         }

//     }, {once : true});


// }

