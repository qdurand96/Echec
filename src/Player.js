import {
    Pawn
} from "./Pawn.js"

export class Player {
    // playerId;
    chessBoard;
    pawns = [];
    color;

    constructor(chessBoard,color) {
        // this.playerId = id;
        this.chessBoard = chessBoard;
        this.color = color;
    }

    createPawn(y, x) {
        let pawn = new Pawn(y, x, this.chessBoard, this.color);
        pawn.create()
        this.pawns.push(pawn);
    }

}