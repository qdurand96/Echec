export class Pawn {
    board;
    y;
    x;
    htmlElement;
    player;
    possibleMove1;
    possibleMove2;
    possibleTake1;
    possibleTake2;

    constructor(y, x, board, player) {
        this.y = y;
        this.x = x;
        this.board = board;
        this.player = player;
        this.possibleMove1 = this.player === 'black' ? this.board[this.y + 1][this.x + 1] : this.board[this.y - 1][this.x - 1];
        this.possibleMove2 = this.player === 'black' ? this.board[this.y + 1][this.x - 1] : this.board[this.y - 1][this.x + 1];
        this.possibleTake1 = this.player === 'black' ? this.board[this.y + 2][this.x + 2] : this.board[this.y - 2][this.x - 2];
        this.possibleTake2 = this.player === 'black' ? this.board[this.y + 2][this.x - 2] : this.board[this.y - 2][this.x + 2];
    }

    create() {
        let element = document.createElement('div');
        this.player === 'black' ? element.classList.add('black-pawn') : element.classList.add('white-pawn');
        this.htmlElement = element;
        this.board[this.y][this.x].appendChild(this.htmlElement);
    }

    firstClick() {
        if (this.possibleMove1.hasChildNodes()) {
            console.log(this.possibleTake1)
            this.possibleTake1.classList.add('possibleMove')
        } else {
            this.possibleMove1.classList.add('possibleMove');
        }

        if (this.possibleMove2.hasChildNodes()) {
            console.log(this.possibleTake2)
            this.possibleTake2.classList.add('possibleMove')
        } else {
            this.possibleMove2.classList.add('possibleMove');
        }
    }

    getPossiblesMoves() {
        this.possibleMove1 = this.player === 'black' ? this.board[this.y + 1][this.x + 1] : this.board[this.y - 1][this.x - 1];
        this.possibleMove2 = this.player === 'black' ? this.board[this.y + 1][this.x - 1] : this.board[this.y - 1][this.x + 1];
        this.possibleTake1 = this.player === 'black' ? this.board[this.y + 2][this.x + 2] : this.board[this.y - 2][this.x - 2];
        this.possibleTake2 = this.player === 'black' ? this.board[this.y + 2][this.x - 2] : this.board[this.y - 2][this.x + 2];
    }

    resetPossiblesMoves() {
        let reset = document.querySelectorAll('td');
        for (let i = 0; i < reset.length; i++) {
            reset[i].classList.remove('possibleMove');
        }
    }

    move1() {
        this.resetPossiblesMoves();
        this.possibleMove1.appendChild(this.htmlElement);
        this.y = this.player === 'black' ? this.y + 1 : this.y - 1;
        this.x = this.player === 'black' ? this.x + 1 : this.x - 1;
        this.getPossiblesMoves();
    }

    move2() {
        this.resetPossiblesMoves();
        this.possibleMove2.appendChild(this.htmlElement);
        this.y = this.player === 'black' ? this.y + 1 : this.y - 1;
        this.x = this.player === 'black' ? this.x - 1 : this.x + 1;
        this.getPossiblesMoves();
    }

    take1() {        
        this.resetPossiblesMoves();
        this.possibleTake1.appendChild(this.htmlElement);
        this.possibleMove1.innerHTML = '';
        this.y = this.player === 'black' ? this.y + 2 : this.y - 2;
        this.x = this.player === 'black' ? this.x + 2 : this.x - 2;
        this.getPossiblesMoves();
    }

    take2() {
        this.resetPossiblesMoves();
        this.possibleTake2.appendChild(this.htmlElement);
        this.possibleMove2.innerHTML = '';
        this.y = this.player === 'black' ? this.y + 2 : this.y - 2;
        this.x = this.player === 'black' ? this.x - 2 : this.x + 2;
        this.getPossiblesMoves();
    }
}