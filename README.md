# Projet jeu de dames

Un projet de jeu d'échec, qui s'est transformé en jeu de dames, non terminé. La maquette et le user case ont été fait pour les échecs.

# Maquette
![alt text](public/img/Echec.png)

# User case
![alt text](public/img/EchecUserCase.png)

**Lien:** <https://qdurand96.gitlab.io/Echec>